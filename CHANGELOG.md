# 0.1.0-beta-3

* Add support for config file validation (#35).
* Fix bug with low row argument having `usize` overflow (#34).

# 0.1.0-beta-2

* Add integer and float expressions (#14).
* Support CDFs with references (#27).
* Set and ReferenceSet are combined.
* Support specifying column delimiter (#33).
* Support writing column header with `has_header` (#32).

# 0.1.0-beta-1

Initial pre-release! Many things are still in flux.

