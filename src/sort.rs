use anyhow::{anyhow, Context, Result};
use rayon::prelude::*;
use std::cmp::Ordering;
use std::fs::remove_file;
use std::path::Path;

use crate::config::VerboseConfig;

pub fn sort_file(config: &VerboseConfig) -> Result<()> {
    let path = Path::new(&config.output_file);

    let column_indices: Result<Vec<usize>> = config
        .column_options
        .sort_column_names
        .iter()
        .map(|sort_column_name| {
            let column_names = config.get_column_names();
            let matched_column = column_names
                .iter()
                .enumerate()
                .find(move |(_, c)| **c == sort_column_name);
            if let Some((index, _)) = matched_column {
                Ok(index)
            } else {
                Err(anyhow!("Sort column {} was not found.", sort_column_name))
            }
        })
        .collect();

    if let Err(e) = column_indices {
        return Err(e);
    }

    let column_indices: Vec<usize> = column_indices.unwrap();

    let mut rows: Vec<csv::StringRecord> = vec![];

    // Open a block so file reference is dropped after CSV reading is done
    {
        let mut rdr = csv::ReaderBuilder::new()
            .delimiter(config.column_delimiter)
            .from_path(path)
            .with_context(|| format!("Failed to open output file at {:?}", path))?;

        for result in rdr.records() {
            let record: csv::StringRecord = result?;
            rows.push(record);
        }
    }

    rows.par_sort_unstable_by(|a, b| {
        for i in column_indices.iter() {
            let a_col = &a[*i];
            let b_col = &b[*i];

            let cmp = a_col.cmp(b_col);
            if cmp != Ordering::Equal {
                return cmp;
            };
        }
        Ordering::Equal
    });

    remove_file(path)?;

    let mut writer = csv::WriterBuilder::new()
        .delimiter(config.column_delimiter)
        .from_path(&path)?;
    if config.has_header {
        writer.write_record(config.get_column_names())?;
    }
    for row in rows {
        writer.write_record(&row)?;
    }

    Ok(())
}
