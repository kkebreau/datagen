// External
use anyhow::{anyhow, Context, Result};
use clap::ArgMatches;
use crossbeam_channel::{bounded, Receiver, Sender};
use log::*;
use rand::thread_rng;
use std::fs::File;
use std::io::Write;
use std::path::Path;
use std::sync::{Arc, Mutex};

// Internal
use crate::column::Column;
use crate::config::{NestedVerboseConfig, VerboseConfig};
use crate::constants::DEFAULT_ROW_DELIMITER;
use crate::gen::{build_closure, WriteColumnClosure};
use crate::profile::profile;
use crate::sort::sort_file;

// 1 MB
const DEFAULT_BUFFER_LENGTH: usize = 1_000_000;

// This needs to be long enough so we are sure to not overflow the buffer
const MAX_ROW_SIZE: usize = 1_000;

// Currently, we expect all messages to be handled before proceeding, so no need for the channel to
// contain many messages.
const CHANNEL_CAPACITY: usize = 1;

pub fn handle_output_command<'a>(matches: &ArgMatches<'a>) -> Result<()> {
    let commands = parse_output_command(matches)?;

    for c in commands {
        handle_single_output_command(&c)?;
    }

    Ok(())
}

fn handle_single_output_command(config: &VerboseConfig) -> Result<()> {
    // Get closures for generating column values
    let mut closures: Vec<WriteColumnClosure> = vec![];
    for (name, type_info) in config.column_options.columns.iter() {
        let column = Column::new(name.to_owned(), type_info.to_owned());
        let closure = build_closure(&column, &config)?;
        closures.push(closure);
    }

    profile(
        &format!(
            "Generating {} rows from config file {}",
            &config.rows, &config.output_file
        ),
        || process_all(&config, closures, DEFAULT_BUFFER_LENGTH),
    )
}

fn process_all(
    config: &VerboseConfig,
    closures: Vec<WriteColumnClosure>,
    buffer_length: usize,
) -> Result<()> {
    let arc_closures = Arc::new(closures);
    let num_columns = config.column_options.columns.len();

    // We only support single-threaded operation if using a key column. This could be optimized in
    // the future by using a concurrent hash table, or generating the values in a single thread and
    // then allowing them to be "owned" by other threads.
    let num_threads = if !config.column_options.key_column_names.is_empty() {
        1
    } else {
        num_cpus::get()
    };

    // Perform final processing
    let thread_work = get_thread_work(num_threads, config.rows);

    // Writing to final file in parent thread rather than stitching things together in the end
    // results in better performance, especially with slow disks.
    let (tx, rx): (Sender<ChildMessage>, Receiver<ChildMessage>) = bounded(CHANNEL_CAPACITY);
    let mut handles = vec![];

    for work in thread_work.iter().filter(|t| t.num_rows > 0) {
        let thread_closures = arc_closures.clone();
        let work = work.clone();
        let tx = tx.clone();
        let column_delimiter = config.column_delimiter;
        let thread = std::thread::spawn(move || {
            let buffer: Vec<u8> = vec![0; buffer_length];
            let arc = Arc::new(Mutex::new(buffer));
            profile(
                &format!("Processing {} rows in thread {}", work.num_rows, work.index),
                || {
                    work.tx.send(ParentMessage::BufferReady).unwrap();
                    if let Err(e) = process_single_thread(
                        column_delimiter,
                        thread_closures,
                        num_columns,
                        work,
                        arc,
                        tx,
                    ) {
                        panic!(e);
                    }
                },
            )
        });

        handles.push(thread);
    }

    let active_threads = num_threads - thread_work.iter().filter(|t| t.num_rows == 0).count();
    let mut done_threads = 0;
    let output_file = Path::new(&config.output_file);
    let mut file = File::create(output_file)?;

    write_header(&config, &mut file)?;

    loop {
        if let Ok(m) = rx.try_recv() {
            match m {
                ChildMessage::BufferWritten {
                    bytes_written,
                    arc,
                    index,
                } => {
                    let buffer = arc.lock().unwrap();
                    file.write_all(&buffer[..bytes_written])?;
                    // Notify the worker that it's safe to use the buffer again.
                    thread_work[index].tx.send(ParentMessage::BufferReady)?;
                }
                ChildMessage::Done => {
                    done_threads += 1;
                }
            }
        }
        if done_threads == active_threads {
            break;
        }
    }

    while let Some(handle) = handles.pop() {
        if let Err(e) = handle.join() {
            return Err(anyhow!(
                "Encountered an unexpected error in one of the threads: {:?}",
                e
            ));
        }
    }

    info!("Generated {} rows in {:?}", config.rows, output_file);

    // handle sorting
    if !config.column_options.sort_column_names.is_empty() {
        profile("Sorting output file", || sort_file(&config))?;
    }

    Ok(())
}

pub fn write_header(config: &VerboseConfig, file: &mut File) -> Result<()> {
    if config.has_header {
        for (index, name) in config.get_column_names().iter().enumerate() {
            let bytes = name.as_bytes();
            file.write_all(bytes)?;
            if index < config.column_options.columns.len() - 1 {
                file.write_all(&[config.column_delimiter])?;
            } else {
                file.write_all(&[DEFAULT_ROW_DELIMITER])?;
            }
        }
    }

    Ok(())
}

fn parse_output_command<'a>(matches: &ArgMatches<'a>) -> Result<Vec<VerboseConfig>> {
    let file = matches.value_of("layout_file").unwrap();
    let file = Path::new(file);

    let config = NestedVerboseConfig::from_path(&file)
        .with_context(|| format!("Failed to parse file {:?}", file))?;
    Ok(config.files)
}

#[derive(Clone)]
struct ThreadWork {
    index: usize,
    num_rows: usize,
    rx: Receiver<ParentMessage>,
    tx: Sender<ParentMessage>,
}

fn get_thread_work(degree_of_parallelism: usize, total_rows: usize) -> Vec<ThreadWork> {
    let (leftover_rows, rows_per_thread) = if degree_of_parallelism > total_rows {
        (total_rows, 0)
    } else {
        let rows_per_thread = total_rows / degree_of_parallelism;
        (
            total_rows - rows_per_thread * degree_of_parallelism,
            rows_per_thread,
        )
    };

    (0..degree_of_parallelism)
        .into_iter()
        .map(|i| {
            let (tx, rx): (Sender<ParentMessage>, Receiver<ParentMessage>) =
                bounded(CHANNEL_CAPACITY);
            ThreadWork {
                index: i,
                num_rows: rows_per_thread + if i == 0 { leftover_rows } else { 0 },
                tx,
                rx,
            }
        })
        .collect()
}

// Message sent by worker threads
enum ChildMessage {
    BufferWritten {
        bytes_written: usize,
        arc: Arc<Mutex<Vec<u8>>>,
        index: usize,
    },
    Done,
}

// Message sent by parent thread
enum ParentMessage {
    BufferReady,
}

fn process_single_thread(
    column_delimiter: u8,
    closures: Arc<Vec<WriteColumnClosure>>,
    num_columns: usize,
    work: ThreadWork,
    arc: Arc<Mutex<Vec<u8>>>,
    tx: Sender<ChildMessage>,
) -> Result<()> {
    let mut rng = thread_rng();
    let mut bytes_written = 0;
    let mut column_values = vec![vec![]; num_columns];
    let mut rows_generated = 0;

    loop {
        if let Ok(ParentMessage::BufferReady) = work.rx.try_recv() {
            let mut buffer = arc.lock().unwrap();
            'inner: loop {
                for c in closures.iter() {
                    c(
                        &mut rng,
                        &mut buffer,
                        &mut bytes_written,
                        &mut column_values,
                        &mut rows_generated,
                    );
                    buffer[bytes_written] = column_delimiter;
                    bytes_written += 1;
                }
                buffer[bytes_written - 1] = DEFAULT_ROW_DELIMITER;
                rows_generated += 1;

                if buffer.len() - bytes_written < MAX_ROW_SIZE || rows_generated == work.num_rows {
                    break 'inner;
                }
            }
        } else {
            continue;
        }

        tx.send(ChildMessage::BufferWritten {
            bytes_written,
            arc: arc.clone(),
            index: work.index,
        })
        .unwrap();
        bytes_written = 0;

        if rows_generated == work.num_rows {
            break;
        }
    }

    tx.send(ChildMessage::Done).unwrap();

    Ok(())
}
