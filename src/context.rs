// External
use anyhow::{anyhow, Context, Result};
use chrono::{NaiveDate, NaiveDateTime, NaiveTime};
use rand::distributions::WeightedIndex;
use std::collections::HashMap;
use std::path::Path;

// Internal
use crate::config::MissingReferenceBehavior;
use crate::config::VerboseConfig;
use crate::type_info::{F64Expression, I64Expression, TypeInfo};
use crate::util::build_reference_key_csv;

#[derive(Debug)]
/// Object containing state which can be captured by the data generating closures
pub enum ClosureContext {
    Boolean {
        probability: f64,
    },
    Float {
        min: f64,
        max: f64,
    },
    ContinuousDistribution {
        distribution: WeightedIndex<f64>,
        // (min, max)
        ranges: Vec<(f64, f64)>,
    },
    ReferenceContinuousDistribution {
        column_indices: Vec<usize>,
        missing_reference_behavior: MissingReferenceBehavior,
        distributions: HashMap<String, RangeDistribution>,
    },
    Datetime {
        min: NaiveDateTime,
        max: NaiveDateTime,
    },
    Date {
        min: NaiveDate,
        max: NaiveDate,
    },
    Time {
        min: NaiveTime,
        max: NaiveTime,
    },
    Integer {
        min: i64,
        max: i64,
    },
    I64Expression(I64Expression),
    F64Expression(F64Expression),
    Set {
        items: Vec<String>,
        distribution: WeightedIndex<f64>,
    },
    ReferenceSet {
        column_indices: Vec<usize>,
        missing_reference_behavior: MissingReferenceBehavior,
        distributions: ReferenceMap<StringDistribution>,
    },
}

impl ClosureContext {
    pub fn new(type_info: TypeInfo, config: &VerboseConfig) -> Result<ClosureContext> {
        let VerboseConfig {
            missing_reference_behavior,
            ..
        } = config;
        let column_names: Vec<&String> = config.get_column_names();
        let context = match type_info {
            TypeInfo::Boolean { probability } => ClosureContext::Boolean { probability },
            TypeInfo::Float { min, max } => ClosureContext::Float { min, max },
            TypeInfo::Integer { min, max } => ClosureContext::Integer { min, max },
            TypeInfo::Date { min, max } => ClosureContext::Date { min, max },
            TypeInfo::Time { min, max } => ClosureContext::Time { min, max },
            TypeInfo::Datetime { min, max } => ClosureContext::Datetime { min, max },
            TypeInfo::Set {
                path,
                column_index,
                weight_index,
                reference_columns,
            } => {
                if let Some(reference_columns) = reference_columns {
                    let (column_indices, missing_reference_behavior, distributions) =
                        get_reference_set_context(
                            &path,
                            column_index,
                            weight_index,
                            column_names,
                            missing_reference_behavior,
                            reference_columns,
                        )?;
                    ClosureContext::ReferenceSet {
                        column_indices,
                        missing_reference_behavior,
                        distributions,
                    }
                } else {
                    let (items, distribution) = get_set_context(&path, column_index, weight_index)?;
                    ClosureContext::Set {
                        items,
                        distribution,
                    }
                }
            }
            TypeInfo::ContinuousDistribution {
                path,
                min_index,
                max_index,
                weight_index,
                reference_columns,
            } => {
                if let Some(reference_columns) = reference_columns {
                    let (column_indices, missing_reference_behavior, distributions) =
                        get_reference_cdf_context(
                            &path,
                            min_index,
                            max_index,
                            weight_index,
                            column_names,
                            missing_reference_behavior,
                            reference_columns,
                        )?;
                    ClosureContext::ReferenceContinuousDistribution {
                        column_indices,
                        missing_reference_behavior,
                        distributions,
                    }
                } else {
                    let (ranges, distribution) = get_continuous_distribution_context(&path)?;
                    ClosureContext::ContinuousDistribution {
                        ranges,
                        distribution,
                    }
                }
            }
            TypeInfo::I64Expression(exp) => ClosureContext::I64Expression(exp),
            TypeInfo::F64Expression(exp) => ClosureContext::F64Expression(exp),
        };

        Ok(context)
    }
}

type StringDistribution = (Vec<String>, WeightedIndex<f64>);
type RangeDistribution = (Vec<(f64, f64)>, WeightedIndex<f64>);
type ReferenceMap<T> = HashMap<String, T>;

fn get_set_context(
    path: &str,
    column_index: usize,
    weight_index: Option<usize>,
) -> Result<StringDistribution> {
    let path = Path::new(path);
    let mut reader =
        csv::Reader::from_path(path).with_context(|| format!("Error reading file {:?}", path))?;
    let mut items: Vec<String> = vec![];
    let mut weights: Vec<f64> = vec![];

    for result in reader.records() {
        let record: csv::StringRecord = result?;
        let weight = if let Some(index) = weight_index {
            record[index].parse()?
        } else {
            1.0
        };
        if let Some(item) = record.get(column_index) {
            items.push(item.to_owned());
        } else {
            return Err(anyhow!(
                "Record {:?} has no value for column index {}",
                record,
                column_index
            ));
        }

        weights.push(weight);
    }

    let distribution = WeightedIndex::new(weights)?;

    Ok((items, distribution))
}

fn get_reference_set_context(
    path: &str,
    column_index: usize,
    weight_index: Option<usize>,
    column_names: Vec<&String>,
    missing_reference_behavior: &MissingReferenceBehavior,
    reference_columns: Vec<(String, usize)>,
) -> Result<(
    Vec<usize>,
    MissingReferenceBehavior,
    ReferenceMap<StringDistribution>,
)> {
    let path = Path::new(&path);
    let mut reader =
        csv::Reader::from_path(path).with_context(|| format!("Error reading file {:?}", path))?;

    // column indices of reference columns in the weights file
    let reference_column_indices: Vec<usize> = reference_columns.iter().map(|(_, i)| *i).collect();

    let mut distributions: HashMap<String, (Vec<String>, Vec<f64>)> = HashMap::new();
    for record in reader.records() {
        let record: csv::StringRecord = record?;
        let key = build_reference_key_csv(&reference_column_indices, &record);
        let item = record[column_index].to_owned();
        let weight = if let Some(index) = weight_index {
            record[index].parse()?
        } else {
            1.0
        };

        match distributions.get_mut(&key) {
            Some((items, weights)) => {
                items.push(item);
                weights.push(weight);
            }
            None => {
                distributions.insert(key, (vec![item], vec![weight]));
            }
        }
    }

    let distributions = distributions
        .into_iter()
        .map(|(k, v)| {
            let (items, weights) = v;
            let distribution = WeightedIndex::new(weights).unwrap();
            (k, (items, distribution))
        })
        .collect();

    // Column indices of reference columns in the intermediate buffer
    let buffer_column_indices: Result<Vec<usize>> = reference_columns
        .iter()
        .map(|(name, _)| {
            if let Some((index, _)) = column_names.iter().enumerate().find(|(_, n)| *n == &name) {
                Ok(index)
            } else {
                Err(anyhow!("Column name {} was not present.", name))
            }
        })
        .collect();

    Ok((
        buffer_column_indices?,
        missing_reference_behavior.to_owned(),
        distributions,
    ))
}

fn get_continuous_distribution_context(path: &str) -> Result<RangeDistribution> {
    let path = Path::new(&path);
    let mut reader =
        csv::Reader::from_path(path).with_context(|| format!("Error reading file {:?}", path))?;

    let mut ranges: Vec<(f64, f64)> = vec![];
    let mut weights: Vec<f64> = vec![];
    for result in reader.records() {
        let record: csv::StringRecord = result?;
        let min: f64 = record[0].parse()?;
        let max: f64 = record[1].parse()?;
        let weight: f64 = record[2].parse()?;

        ranges.push((min, max));
        weights.push(weight);
    }

    let distribution = WeightedIndex::new(weights)?;

    Ok((ranges, distribution))
}

fn get_reference_cdf_context(
    path: &str,
    min_index: usize,
    max_index: usize,
    weight_index: usize,
    column_names: Vec<&String>,
    missing_reference_behavior: &MissingReferenceBehavior,
    reference_columns: Vec<(String, usize)>,
) -> Result<(
    Vec<usize>,
    MissingReferenceBehavior,
    ReferenceMap<RangeDistribution>,
)> {
    let path = Path::new(&path);
    let mut reader =
        csv::Reader::from_path(path).with_context(|| format!("Error reading file {:?}", path))?;

    // column indices of reference columns in the weights file
    let reference_column_indices: Vec<usize> = reference_columns.iter().map(|(_, i)| *i).collect();

    let mut distributions: HashMap<String, (Vec<(f64, f64)>, Vec<f64>)> = HashMap::new();
    for record in reader.records() {
        let record: csv::StringRecord = record?;
        let key = build_reference_key_csv(&reference_column_indices, &record);
        let min: f64 = record[min_index].parse()?;
        let max: f64 = record[max_index].parse()?;
        let weight: f64 = record[weight_index].parse()?;

        match distributions.get_mut(&key) {
            Some((items, weights)) => {
                items.push((min, max));
                weights.push(weight);
            }
            None => {
                distributions.insert(key, (vec![(min, max)], vec![weight]));
            }
        }
    }

    let distributions = distributions
        .into_iter()
        .map(|(k, v)| {
            let (items, weights) = v;
            let distribution = WeightedIndex::new(weights).unwrap();
            (k, (items, distribution))
        })
        .collect();

    // Column indices of reference columns in the intermediate buffer
    let buffer_column_indices: Result<Vec<usize>> = reference_columns
        .iter()
        .map(|(name, _)| {
            if let Some((index, _)) = column_names.iter().enumerate().find(|(_, n)| *n == &name) {
                Ok(index)
            } else {
                Err(anyhow!("Column name {} was not present.", name))
            }
        })
        .collect();

    Ok((
        buffer_column_indices?,
        missing_reference_behavior.to_owned(),
        distributions,
    ))
}
