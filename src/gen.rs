// External
use anyhow::{anyhow, Result};
use chrono::{Datelike, NaiveDate, NaiveDateTime, NaiveTime, Timelike};
use rand::prelude::*;
use rand::rngs::ThreadRng;
use rand::Rng;
use std::collections::HashSet;

// Internal
use crate::column::Column;
use crate::config::MissingReferenceBehavior;
use crate::config::VerboseConfig;
use crate::constants::{DATETIME_FORMAT, DATE_FORMAT, TIME_FORMAT};
use crate::context::ClosureContext;
use crate::type_info::{F64Expression, I64Expression, Opcode};
use crate::util::build_reference_key;

pub type WriteColumnClosure =
    Box<dyn Fn(&mut ThreadRng, &mut [u8], &mut usize, &mut Vec<Vec<u8>>, &mut usize) + Send + Sync>;

/// Construct a closure representing instructions to execute. Each closure should:
/// 1. Generate a new random value for the associated column type withing the specified
///    constraints.
/// 2. Write the bytes to the output buffer passed into the closure.
/// 3. Increment the `bytes_written` counter accordingly.
/// 4. If the column is used as a reference source of a `NestedWeightedIndex` column, record the
///    bytes written to a row-level cache, `columns`.

pub fn build_closure(column: &Column, config: &VerboseConfig) -> Result<WriteColumnClosure> {
    let Column { type_info, .. } = column;
    let context = ClosureContext::new(type_info.to_owned(), &config)?;
    let should_copy = column.should_copy(&config);
    let is_key = column.is_key(&config);
    let index = column.index(&config);
    let rows = config.rows;
    match context {
        ClosureContext::Boolean { probability } => {
            get_boolean_closure(probability, should_copy, index)
        }
        ClosureContext::Float { min, max } => {
            Ok(Box::new(move |rng, buffer, bytes_written, columns, _| {
                let num: f64 = rng.gen_range(min, max);
                let mem = format!("{:.2}", num);
                let mem = mem.as_bytes();

                if should_copy {
                    columns[index] = mem.to_vec();
                }

                buffer[*bytes_written..*bytes_written + mem.len()].copy_from_slice(&mem);
                *bytes_written += mem.len();
            }))
        }
        ClosureContext::Integer { min, max } => {
            if is_key {
                get_integer_key_bytes_closure(min, max, rows)
            } else {
                Ok(Box::new(move |rng, buffer, bytes_written, columns, _| {
                    let num: i64 = rng.gen_range(min, max);
                    let mem = num.to_string();
                    let mem = mem.as_bytes();

                    if should_copy {
                        columns[index] = mem.to_vec();
                    }

                    buffer[*bytes_written..*bytes_written + mem.len()].copy_from_slice(&mem);
                    *bytes_written += mem.len();
                }))
            }
        }
        ClosureContext::Time { min, max } => {
            if min > max {
                return Err(anyhow!("min ({}) was greater than max ({})", min, max));
            }
            let min_seconds = min.num_seconds_from_midnight();
            let max_seconds = max.num_seconds_from_midnight();
            Ok(Box::new(move |rng, buffer, bytes_written, columns, _| {
                let num = rng.gen_range(min_seconds, max_seconds);
                let time = NaiveTime::from_num_seconds_from_midnight(num, 0);
                let mem = time.format(&TIME_FORMAT).to_string();
                let mem = mem.as_bytes();

                if should_copy {
                    columns[index] = mem.to_vec();
                }

                buffer[*bytes_written..*bytes_written + mem.len()].copy_from_slice(&mem);
                *bytes_written += mem.len();
            }))
        }
        ClosureContext::Datetime { min, max } => {
            let min_timestamp = min.timestamp();
            let max_timestamp = max.timestamp();
            Ok(Box::new(move |rng, buffer, bytes_written, columns, _| {
                let num = rng.gen_range(min_timestamp, max_timestamp);
                let date = NaiveDateTime::from_timestamp(num, 0);
                let mem = date.format(&DATETIME_FORMAT).to_string();
                let mem = mem.as_bytes();

                if should_copy {
                    columns[index] = mem.to_vec();
                }

                buffer[*bytes_written..*bytes_written + mem.len()].copy_from_slice(&mem);
                *bytes_written += mem.len();
            }))
        }
        ClosureContext::Date { min, max } => {
            let min_days = min.num_days_from_ce();
            let max_days = max.num_days_from_ce();
            Ok(Box::new(move |rng, buffer, bytes_written, columns, _| {
                let num = rng.gen_range(min_days, max_days);
                let date = NaiveDate::from_num_days_from_ce(num);
                let mem = date.format(&DATE_FORMAT).to_string();
                let mem = mem.as_bytes();

                if should_copy {
                    columns[index] = mem.to_vec();
                }

                buffer[*bytes_written..*bytes_written + mem.len()].copy_from_slice(&mem);
                *bytes_written += mem.len();
            }))
        }
        ClosureContext::Set {
            distribution,
            items,
        } => Ok(Box::new(move |rng, buffer, bytes_written, columns, _| {
            let item = items[distribution.sample(rng)].clone();
            let mem = item.as_bytes();

            if should_copy {
                columns[index] = mem.to_vec();
            }

            buffer[*bytes_written..*bytes_written + mem.len()].copy_from_slice(&mem);
            *bytes_written += mem.len();
        })),
        ClosureContext::ReferenceSet {
            distributions,
            column_indices,
            missing_reference_behavior,
        } => {
            let missing_reference_closure = match missing_reference_behavior {
                MissingReferenceBehavior::FillWithNull => |_| NULL_BYTES,
                MissingReferenceBehavior::FillWithEmpty => |_| EMPTY_BYTES,
                MissingReferenceBehavior::Panic => {
                    |key| panic!("No matching reference found for key {:?}", key)
                }
            };
            Ok(Box::new(move |rng, buffer, bytes_written, columns, _| {
                let key = build_reference_key(&column_indices, columns);
                let distribution = distributions.get(&key);
                let mem = if let Some((items, weights)) = distribution {
                    items[weights.sample(rng)].as_bytes()
                } else {
                    missing_reference_closure(key)
                };
                buffer[*bytes_written..*bytes_written + mem.len()].copy_from_slice(&mem);
                *bytes_written += mem.len();

                if should_copy {
                    columns[index] = mem.to_vec();
                }
            }))
        }
        ClosureContext::ContinuousDistribution {
            distribution,
            ranges,
        } => Ok(Box::new(move |rng, buffer, bytes_written, columns, _| {
            let (min, max) = ranges[distribution.sample(rng)];
            let num: f64 = rng.gen_range(min, max);
            let mem = format!("{:.2}", num);
            let mem = mem.as_bytes();

            if should_copy {
                columns[index] = mem.to_vec();
            }

            buffer[*bytes_written..*bytes_written + mem.len()].copy_from_slice(&mem);
            *bytes_written += mem.len();
        })),
        ClosureContext::ReferenceContinuousDistribution {
            distributions,
            column_indices,
            missing_reference_behavior,
        } => {
            let missing_reference_closure = match missing_reference_behavior {
                MissingReferenceBehavior::FillWithNull => |_| NULL_BYTES,
                MissingReferenceBehavior::FillWithEmpty => |_| EMPTY_BYTES,
                MissingReferenceBehavior::Panic => {
                    |key| panic!("No matching reference found for key {:?}", key)
                }
            };
            Ok(Box::new(move |rng, buffer, bytes_written, columns, _| {
                let key = build_reference_key(&column_indices, columns);
                let distribution = distributions.get(&key);
                if let Some((ranges, weights)) = distribution {
                    let (min, max) = ranges[weights.sample(rng)];
                    let num: f64 = rng.gen_range(min, max);
                    let mem = format!("{:.2}", num);
                    let mem = mem.as_bytes();
                    buffer[*bytes_written..*bytes_written + mem.len()].copy_from_slice(&mem);
                    *bytes_written += mem.len();
                    if should_copy {
                        columns[index] = mem.to_vec();
                    }
                } else {
                    let mem = missing_reference_closure(key);
                    buffer[*bytes_written..*bytes_written + mem.len()].copy_from_slice(&mem);
                    *bytes_written += mem.len();
                    if should_copy {
                        columns[index] = mem.to_vec();
                    }
                };
            }))
        }
        ClosureContext::I64Expression(expr) => {
            let get_value_closure = materialize_i64_expression(expr, &config);
            Ok(Box::new(move |rng, buffer, bytes_written, columns, _| {
                let num = get_value_closure(rng, columns);
                let mem = num.to_string();
                let mem = mem.as_bytes();

                if should_copy {
                    columns[index] = mem.to_vec();
                }

                buffer[*bytes_written..*bytes_written + mem.len()].copy_from_slice(&mem);
                *bytes_written += mem.len();
            }))
        }
        ClosureContext::F64Expression(expr) => {
            let get_value_closure = materialize_f64_expression(expr, &config);
            Ok(Box::new(move |rng, buffer, bytes_written, columns, _| {
                let num = get_value_closure(rng, columns);
                let mem = format!("{:.2}", num);
                let mem = mem.as_bytes();

                if should_copy {
                    columns[index] = mem.to_vec();
                }

                buffer[*bytes_written..*bytes_written + mem.len()].copy_from_slice(&mem);
                *bytes_written += mem.len();
            }))
        }
    }
}

fn materialize_i64_expression(
    expr: I64Expression,
    config: &VerboseConfig,
) -> Box<dyn Fn(&mut ThreadRng, &mut Vec<Vec<u8>>) -> i64 + Send + Sync> {
    match expr {
        I64Expression::RandomInteger { min, max } => {
            Box::new(move |rng, _| rng.gen_range(min, max))
        }
        I64Expression::Number(n) => Box::new(move |_, _| n),
        I64Expression::Binary(l, op, r) => {
            let left = materialize_i64_expression(*l, config);
            let right = materialize_i64_expression(*r, config);

            match op {
                Opcode::Mul => Box::new(move |r, columns| left(r, columns) * right(r, columns)),
                Opcode::Div => Box::new(move |r, columns| left(r, columns) / right(r, columns)),
                Opcode::Add => Box::new(move |r, columns| left(r, columns) + right(r, columns)),
                Opcode::Sub => Box::new(move |r, columns| left(r, columns) - right(r, columns)),
            }
        }
        I64Expression::Reference(column_name) => {
            let (index, _) = config
                .get_column_names()
                .iter()
                .enumerate()
                .find(|(_, n)| **n == &column_name)
                .unwrap();
            Box::new(move |_, columns| {
                let value = String::from_utf8(columns[index].to_owned()).unwrap();
                value.parse().unwrap()
            })
        }
    }
}

fn materialize_f64_expression(
    expr: F64Expression,
    config: &VerboseConfig,
) -> Box<dyn Fn(&mut ThreadRng, &mut Vec<Vec<u8>>) -> f64 + Send + Sync> {
    match expr {
        F64Expression::RandomFloat { min, max } => Box::new(move |rng, _| rng.gen_range(min, max)),
        F64Expression::Number(n) => Box::new(move |_, _| n),
        F64Expression::Binary(l, op, r) => {
            let left = materialize_f64_expression(*l, config);
            let right = materialize_f64_expression(*r, config);

            match op {
                Opcode::Mul => Box::new(move |r, columns| left(r, columns) * right(r, columns)),
                Opcode::Div => Box::new(move |r, columns| left(r, columns) / right(r, columns)),
                Opcode::Add => Box::new(move |r, columns| left(r, columns) + right(r, columns)),
                Opcode::Sub => Box::new(move |r, columns| left(r, columns) - right(r, columns)),
            }
        }
        F64Expression::Reference(column_name) => {
            let (index, _) = config
                .get_column_names()
                .iter()
                .enumerate()
                .find(|(_, n)| **n == &column_name)
                .unwrap();
            Box::new(move |_, columns| {
                let value = String::from_utf8(columns[index].to_owned()).unwrap();
                value.parse().unwrap()
            })
        }
    }
}

fn get_boolean_closure(
    probability: f64,
    should_copy: bool,
    index: usize,
) -> Result<WriteColumnClosure> {
    if should_copy {
        Ok(Box::new(move |rng, buffer, bytes_written, columns, _| {
            let mem = rng.gen_bool(probability).to_string();
            let mem = mem.as_bytes();
            buffer[*bytes_written..*bytes_written + mem.len()].copy_from_slice(&mem);
            *bytes_written += mem.len();
            columns[index] = mem.to_owned();
        }))
    } else {
        Ok(Box::new(move |rng, buffer, bytes_written, _, _| {
            let mem = rng.gen_bool(probability).to_string();
            let mem = mem.as_bytes();
            buffer[*bytes_written..*bytes_written + mem.len()].copy_from_slice(&mem);
            *bytes_written += mem.len();
        }))
    }
}

// Try up to this many times before giving up (which would suggest the range for generating
// distinct values is too small)
const KEY_CREATION_ATTEMPTS_PER_ITERATION: usize = 10;

const NULL: &str = "NULL";
const NULL_BYTES: &[u8] = NULL.as_bytes();
const EMPTY_BYTES: &[u8] = &[];

/// Construct a closure representing instructions to execute for creating a key column of the
/// specified number of rows. Each closure should:
/// 1. Generate a new random value for the associated column type withing the specified
///    constraints.
/// 2. Write the bytes to the output buffer passed into the closure.
/// 3. Increment the `bytes_written` counter accordingly.
/// 4. If the column is used as a reference source of a `NestedWeightedIndex` column, record the
///    bytes written to a row-level cache, `columns`.
pub fn get_integer_key_bytes_closure(
    min: i64,
    max: i64,
    rows: usize,
) -> Result<WriteColumnClosure> {
    // Unlike normal closure generation, we generate the random values _before_ entering
    // the main closure execution loop. This ensures that we have a distinct set of the
    // desired size up front. An alternative approach would be to generate them on-the-fly,
    // but this would require passing around the HashSet as a closure argument (as it would
    // need to mutate in the course of execution and thus could not be embedded in the
    // closure). Another advantage of doing this work up front is that it allows us to do
    // less work in the event that the int range is too small for the number of distinct
    // values desired.
    let mut local_rng = thread_rng();
    let mut distinct_values: HashSet<i64> = HashSet::new();

    while distinct_values.len() < rows {
        let mut i = 0;
        loop {
            let num: i64 = local_rng.gen_range(min, max);
            if distinct_values.contains(&num) {
                i += 1;
                if i == KEY_CREATION_ATTEMPTS_PER_ITERATION {
                    return Err(
                                anyhow!("Failed to generate unique value after {} attempts within the range ({}, {})",
                                KEY_CREATION_ATTEMPTS_PER_ITERATION, min, max));
                }
            } else {
                distinct_values.insert(num);
                break;
            }
        }
    }

    // Convert to a vec so we can index
    let mut values: Vec<i64> = vec![];
    for v in distinct_values {
        values.push(v);
    }
    let closure: WriteColumnClosure = Box::new(move |_, buffer, bytes_written, _, row_index| {
        let mem = values[*row_index];
        let mem = mem.to_string();
        let mem = mem.as_bytes();

        buffer[*bytes_written..*bytes_written + mem.len()].copy_from_slice(&mem);
        *bytes_written += mem.len();
    });

    Ok(closure)
}
