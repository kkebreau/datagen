// External
use serde::{Deserialize, Serialize};

// Internal
use crate::config::VerboseConfig;
use crate::type_info::{F64Expression, I64Expression, TypeInfo};

/// Format of columns in input files.
#[derive(Debug, PartialEq, Serialize, Deserialize)]
pub struct ColumnExpression {
    pub name: String,
    pub expr: String,
}

/// Format of columns used internally, after parsing.
#[derive(Debug)]
pub struct Column {
    pub name: String,
    pub type_info: TypeInfo,
}

impl Column {
    pub fn new(name: String, type_info: TypeInfo) -> Self {
        Column { name, type_info }
    }

    // Determine whether the current column should be cached to the intermediate row-level buffer.
    pub fn should_copy(&self, config: &VerboseConfig) -> bool {
        let name = &self.name;
        for t in config.get_type_info().iter() {
            match t {
                TypeInfo::Set {
                    reference_columns, ..
                } => {
                    if let Some(reference_columns) = reference_columns {
                        for (c, _) in reference_columns.iter() {
                            if c == name {
                                return true;
                            }
                        }
                    }
                }
                TypeInfo::ContinuousDistribution {
                    reference_columns, ..
                } => {
                    if let Some(reference_columns) = reference_columns {
                        for (c, _) in reference_columns.iter() {
                            if c == name {
                                return true;
                            }
                        }
                    }
                }
                TypeInfo::I64Expression(expr) => {
                    if is_i64_column_referenced(&expr, &name) {
                        return true;
                    }
                }
                TypeInfo::F64Expression(expr) => {
                    if is_f64_column_referenced(&expr, &name) {
                        return true;
                    }
                }
                _ => {}
            }
        }

        false
    }

    pub fn is_key(&self, config: &VerboseConfig) -> bool {
        config
            .column_options
            .key_column_names
            .iter()
            .any(|c| c == &self.name)
    }

    pub fn index(&self, config: &VerboseConfig) -> usize {
        let (index, _) = config
            .get_column_names()
            .iter()
            .enumerate()
            .find(|(_, name)| **name == &self.name)
            .unwrap();
        index
    }
}

fn is_i64_column_referenced(i64expression: &I64Expression, column_name: &str) -> bool {
    match i64expression {
        I64Expression::Binary(left, _, right) => {
            is_i64_column_referenced(&left, column_name)
                || is_i64_column_referenced(&right, column_name)
        }
        I64Expression::Number(_) => false,
        I64Expression::RandomInteger { .. } => false,
        I64Expression::Reference(name) => name == column_name,
    }
}

fn is_f64_column_referenced(f64expression: &F64Expression, column_name: &str) -> bool {
    match f64expression {
        F64Expression::Binary(left, _, right) => {
            is_f64_column_referenced(&left, column_name)
                || is_f64_column_referenced(&right, column_name)
        }
        F64Expression::Number(_) => false,
        F64Expression::RandomFloat { .. } => false,
        F64Expression::Reference(name) => name == column_name,
    }
}
