// External
use anyhow::{anyhow, Context, Result};
use clap::ArgMatches;
use serde::{Deserialize, Serialize};
use std::collections::{HashMap, HashSet};
use std::convert::TryFrom;
use std::fs::File;
use std::io::Read;
use std::path::Path;

// Internal
use crate::column::ColumnExpression;
use crate::constants::DEFAULT_COLUMN_DELIMITER;
use crate::type_info::TypeInfo;

#[derive(Deserialize, Serialize, Debug)]
#[serde(untagged)]
pub enum ParseableConfig {
    Template(TemplateConfig),
    Concise(ConciseConfig),
    Multiple { files: Vec<ParseableConfig> },
}

#[derive(Debug)]
pub struct ColumnOptions {
    pub columns: Vec<(String, TypeInfo)>,

    // If present, specifies the name of a column whose values should be unique
    pub key_column_names: Vec<String>,

    // If present, specifies that the output data should be sorted by the columns specified.
    // Note that this is an in-memory sort that happens after the data is generated, so it requires
    // that the host machine has enough memory to fit the entire dataset in memory.
    pub sort_column_names: Vec<String>,
}

impl ColumnOptions {
    fn new(
        columns: Vec<(String, TypeInfo)>,
        key_column_names: Vec<String>,
        sort_column_names: Vec<String>,
    ) -> Result<Self> {
        let column_names: HashSet<&String> = columns.iter().map(|(name, _)| name).collect();

        let extra_key_columns: Vec<String> = key_column_names
            .iter()
            .filter(|n| !column_names.contains(n))
            .map(|n| n.to_owned())
            .collect();
        if !extra_key_columns.is_empty() {
            return Err(anyhow!(
                "Invalid key column(s): {}",
                extra_key_columns.join(",")
            ));
        }

        let extra_sort_columns: Vec<String> = sort_column_names
            .iter()
            .filter(|n| !column_names.contains(n))
            .map(|n| n.to_owned())
            .collect();
        if !extra_sort_columns.is_empty() {
            return Err(anyhow!(
                "Invalid sort column(s): {}",
                extra_sort_columns.join(",")
            ));
        }

        let mut seen_column_names = HashSet::new();
        let mut duplicate_column_names = HashSet::new();
        for (name, _) in columns.iter() {
            if seen_column_names.contains(name) {
                duplicate_column_names.insert(name);
            }
            seen_column_names.insert(name);
        }

        if !duplicate_column_names.is_empty() {
            let names: Vec<String> = duplicate_column_names
                .into_iter()
                .map(|c| c.to_owned())
                .collect();
            return Err(anyhow!("Duplicate column(s): {}", names.join(",")));
        }

        Ok(ColumnOptions {
            columns,
            key_column_names,
            sort_column_names,
        })
    }
}

#[derive(Debug)]
pub struct VerboseConfig {
    pub rows: usize,
    pub output_file: String,
    pub has_header: bool,
    pub column_delimiter: u8,
    pub column_options: ColumnOptions,

    // If present, specifies what should be done if a weight key is not present (default is panic)
    pub missing_reference_behavior: MissingReferenceBehavior,
}

#[derive(Deserialize, Serialize, Debug)]
pub struct ConciseConfig {
    pub rows: usize,
    pub output_file: String,
    pub columns: Vec<ColumnExpression>,

    // If present, specifies the name of a column whose values should be unique
    pub key_column_name: Option<String>,

    // If present, specifies what should be done if a weight key is not present (default is panic)
    pub missing_reference_behavior: Option<MissingReferenceBehavior>,

    // If present, specifies that the output data should be sorted by the columns specified.
    // Note that this is an in-memory sort that happens after the data is generated, so it requires
    // that the host machine has enough memory to fit the entire dataset in memory.
    pub sort_column_names: Option<Vec<String>>,

    // Whether or not to write the column names as the first line of output. Defaults to false if
    // not provided.
    pub has_header: Option<bool>,

    // If provided, overrides the default column delimiter. If absent, the default is the tab
    // character.
    pub column_delimiter: Option<String>,
}

#[derive(Deserialize, Serialize, Debug)]
pub struct TemplateConfig {
    // These are the only required properties. The rest, if provided, will override the values from
    // the template file.
    pub template_file: String,
    pub output_file: String,

    pub rows: Option<usize>,
    pub columns: Option<Vec<ColumnExpression>>,

    // If present, specifies the name of a column whose values should be unique
    pub key_column_name: Option<String>,

    // If present, specifies what should be done if a weight key is not present (default is panic)
    pub missing_reference_behavior: Option<MissingReferenceBehavior>,

    // If present, specifies that the output data should be sorted by the columns specified.
    // Note that this is an in-memory sort that happens after the data is generated, so it requires
    // that the host machine has enough memory to fit the entire dataset in memory.
    pub sort_column_names: Option<Vec<String>>,

    // Whether or not to write the column names as the first line of output. Defaults to false if
    // not provided.
    pub has_header: Option<bool>,

    // If provided, overrides the default column delimiter. If absent, the default is the tab
    // character.
    pub column_delimiter: Option<String>,
}

#[derive(Debug)]
pub struct NestedVerboseConfig {
    pub files: Vec<VerboseConfig>,
}

#[derive(Deserialize, Serialize, Debug, PartialEq, Clone)]
pub enum MissingReferenceBehavior {
    FillWithNull,
    FillWithEmpty,
    Panic,
}

impl TryFrom<ConciseConfig> for VerboseConfig {
    type Error = anyhow::Error;

    fn try_from(config: ConciseConfig) -> Result<VerboseConfig> {
        let column_names: Vec<String> = config.columns.iter().map(|c| c.name.to_owned()).collect();
        let types: Result<Vec<TypeInfo>> = config
            .columns
            .iter()
            .map(|c| TypeInfo::new(&c.expr))
            .collect();
        let types: Vec<TypeInfo> = types?;

        let missing_reference_behavior = config
            .missing_reference_behavior
            .unwrap_or(MissingReferenceBehavior::Panic);

        let columns: Vec<(String, TypeInfo)> =
            column_names.into_iter().zip(types.into_iter()).collect();

        let key_column_names = if let Some(k) = config.key_column_name {
            vec![k]
        } else {
            vec![]
        };

        let include_header = config.has_header.unwrap_or(false);

        let column_delimiter: u8 = if let Some(cd) = config.column_delimiter {
            let bytes = cd.as_bytes();
            if bytes.len() > 1 {
                return Err(anyhow!(
                    "Could not parse column delimiter {} as an ASCII character.",
                    cd
                ));
            }
            bytes[0]
        } else {
            DEFAULT_COLUMN_DELIMITER
        };

        let sort_column_names = config.sort_column_names.unwrap_or(vec![]);
        let column_options = ColumnOptions::new(columns, key_column_names, sort_column_names)?;
        Ok(VerboseConfig {
            rows: config.rows,
            output_file: config.output_file,
            column_options,
            missing_reference_behavior,
            has_header: include_header,
            column_delimiter,
        })
    }
}

impl TryFrom<TemplateConfig> for VerboseConfig {
    type Error = anyhow::Error;
    fn try_from(current_config: TemplateConfig) -> Result<VerboseConfig> {
        let template_path = Path::new(&current_config.template_file);
        let template_config = NestedVerboseConfig::from_path(template_path)?;

        if template_config.files.len() > 1 {
            return Err(anyhow!(
                "Template file must contain a single output file, found {}",
                template_config.files.len()
            ));
        }

        let template_config = template_config.files.first().unwrap();

        // favor override values but fall back to template
        let rows = current_config.rows.unwrap_or(template_config.rows);
        let missing_reference_behavior = current_config
            .missing_reference_behavior
            .unwrap_or(template_config.missing_reference_behavior.to_owned());
        let sort_column_names = current_config
            .sort_column_names
            .unwrap_or(template_config.column_options.sort_column_names.to_owned());
        let key_column_names = if let Some(k) = current_config.key_column_name {
            vec![k]
        } else {
            template_config.column_options.key_column_names.to_owned()
        };
        let include_header = current_config
            .has_header
            .unwrap_or(template_config.has_header);
        let column_delimiter: u8 = if let Some(cd) = current_config.column_delimiter {
            let bytes = cd.as_bytes();
            if bytes.len() > 1 {
                return Err(anyhow!("Could not parse {} as an ASCII character.", cd));
            }
            bytes[0]
        } else {
            template_config.column_delimiter
        };

        // Parse the columns provided in the current file
        let (types, column_names) = if let Some(columns) = current_config.columns {
            let types: Result<Vec<TypeInfo>> =
                columns.iter().map(|c| TypeInfo::new(&c.expr)).collect();
            let column_names = columns.iter().map(|c| c.name.to_owned()).collect();
            (types?, column_names)
        } else {
            (vec![], vec![])
        };

        // We want all existing columns, with overrides, if any
        // Any columns provided in the current config that were not present in the template config
        // are appended to the end of the list.
        let mut new_columns: HashMap<String, &TypeInfo> = HashMap::new();
        let mut seen_column_names = HashSet::new();
        let mut final_columns: Vec<(String, TypeInfo)> = vec![];

        for (index, c) in column_names.iter().enumerate() {
            new_columns.insert(c.to_owned(), &types[index]);
        }

        for (name, type_info) in &template_config.column_options.columns {
            let name = name.to_owned();
            seen_column_names.insert(name.to_owned());
            if let Some(type_info) = new_columns.get(&name) {
                final_columns.push((name, (*type_info).to_owned()));
            } else {
                final_columns.push((name, (*type_info).to_owned()));
            }
        }

        for name in column_names.iter() {
            if !seen_column_names.contains(name) {
                let type_info = new_columns.get(name).unwrap();
                final_columns.push((name.to_owned(), (*type_info).to_owned()));
            }
        }

        let column_options =
            ColumnOptions::new(final_columns, key_column_names, sort_column_names)?;
        Ok(VerboseConfig {
            column_options,
            rows,
            output_file: current_config.output_file,
            missing_reference_behavior,
            has_header: include_header,
            column_delimiter,
        })
    }
}

impl VerboseConfig {
    pub fn get_column_names(&self) -> Vec<&String> {
        self.column_options
            .columns
            .iter()
            .map(|(name, _)| name)
            .collect()
    }

    pub fn get_type_info(&self) -> Vec<&TypeInfo> {
        self.column_options
            .columns
            .iter()
            .map(|(_, type_info)| type_info)
            .collect()
    }
}

impl NestedVerboseConfig {
    fn from_concise(config: ConciseConfig) -> Result<NestedVerboseConfig> {
        Ok(NestedVerboseConfig {
            files: vec![VerboseConfig::try_from(config)?],
        })
    }

    fn from_template(config: TemplateConfig) -> Result<NestedVerboseConfig> {
        Ok(NestedVerboseConfig {
            files: vec![VerboseConfig::try_from(config)?],
        })
    }

    fn from_multiple(configs: Vec<ParseableConfig>) -> Result<NestedVerboseConfig> {
        let configs: Result<Vec<VerboseConfig>> = configs
            .into_iter()
            .map(|c| match c {
                ParseableConfig::Concise(spec) => Ok(VerboseConfig::try_from(spec)?),
                ParseableConfig::Template(template) => Ok(VerboseConfig::try_from(template)?),
                ParseableConfig::Multiple { .. } => {
                    Err(anyhow!("Multiple configs cannot be nested."))
                }
            })
            .collect();

        Ok(NestedVerboseConfig { files: configs? })
    }

    pub fn from_path(path: &Path) -> Result<NestedVerboseConfig> {
        let mut file =
            File::open(&path).with_context(|| format!("Could not read file at {:?}", path))?;
        let mut text = vec![];
        file.read_to_end(&mut text)
            .with_context(|| format!("Could not read file at {:?}", path))?;

        let text = String::from_utf8(text)?;

        let parsed: std::result::Result<ParseableConfig, toml::de::Error> = toml::from_str(&text);

        match parsed? {
            ParseableConfig::Template(template) => NestedVerboseConfig::from_template(template),
            ParseableConfig::Concise(spec) => NestedVerboseConfig::from_concise(spec),
            ParseableConfig::Multiple { files: specs } => NestedVerboseConfig::from_multiple(specs),
        }
    }
}

pub fn handle_layout_check_command<'a>(matches: &ArgMatches<'a>) -> Result<NestedVerboseConfig> {
    let file = matches.value_of("layout_file").unwrap();
    let file = Path::new(file);

    NestedVerboseConfig::from_path(&file)
}

#[cfg(test)]
mod tests {
    use std::fs::read_dir;
    use std::path::Path;

    use super::*;

    #[test]
    fn all_example_configs_can_be_parsed() {
        let examples_directory_name = String::from("examples");
        let examples_path = Path::new(&examples_directory_name);
        let dir = read_dir(examples_path).unwrap();
        for entry in dir {
            let entry = entry.unwrap();
            let path_buf = entry.path();
            let path = path_buf.as_path();
            println!("{:?}", path);
            if let (Some(ext), Some(stem)) = (path.extension(), path.file_stem()) {
                if ext == "toml" && stem != "using_template" {
                    let result = NestedVerboseConfig::from_path(path);
                    assert!(
                        result.is_ok(),
                        format!("Failed to parse {:?}. Error: {:?}", path, result.err())
                    );
                }
            }
        }
    }

    #[test]
    fn duplicate_column_names_prohibited() {
        let columns = vec![
            ("hello".to_owned(), TypeInfo::Boolean { probability: 0.0 }),
            ("hello".to_owned(), TypeInfo::Boolean { probability: 0.0 }),
        ];

        let result = ColumnOptions::new(columns, vec![], vec![]);
        assert!(result.is_err(), true);
        assert_eq!(
            format!("{}", result.err().unwrap()),
            "Duplicate column(s): hello"
        );
    }

    #[test]
    fn extra_key_columns_prohibited() {
        let columns = vec![("hello".to_owned(), TypeInfo::Boolean { probability: 0.0 })];

        let result =
            ColumnOptions::new(columns, vec!["world".to_owned(), "the".to_owned()], vec![]);
        assert!(result.is_err(), true);
        assert_eq!(
            format!("{}", result.err().unwrap()),
            "Invalid key column(s): world,the"
        );
    }

    #[test]
    fn extra_sort_columns_prohibited() {
        let columns = vec![("hello".to_owned(), TypeInfo::Boolean { probability: 0.0 })];

        let result =
            ColumnOptions::new(columns, vec![], vec!["world".to_owned(), "the".to_owned()]);
        assert!(result.is_err(), true);
        assert_eq!(
            format!("{}", result.err().unwrap()),
            "Invalid sort column(s): world,the"
        );
    }
}
