use csv::StringRecord;

// Importantly, this needs to not exist in the input data
const DELIMITER_BYTE: u8 = b'\t';
const DELIMITER_CHAR: char = '\t';

pub fn build_reference_key(indices: &[usize], columns: &[Vec<u8>]) -> String {
    let mut output: Vec<u8> = vec![];
    let column = &columns[indices[0]];
    output.extend(column);
    for i in indices.iter().skip(1) {
        let column = &columns[*i];
        output.push(DELIMITER_BYTE);
        output.extend(column);
    }

    String::from_utf8(output).unwrap()
}

pub fn build_reference_key_csv(indices: &[usize], columns: &StringRecord) -> String {
    let mut output: String = String::new();
    let column = &columns[indices[0]];
    output.push_str(column);
    for i in indices.iter().skip(1) {
        let column = &columns[*i];
        output.push(DELIMITER_CHAR);
        output.push_str(column);
    }

    output
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn build_reference_string_basic() {
        let indices = vec![0, 1, 2];
        let columns = vec![vec![2, 3, 4], vec![5, 6, 7], vec![8, 9, 10], vec![1, 2, 3]];

        let result = build_reference_key(&indices, &columns);
        assert_eq!(
            &result.as_bytes(),
            &vec![2, 3, 4, DELIMITER_BYTE, 5, 6, 7, DELIMITER_BYTE, 8, 9, 10]
        );
    }
}
