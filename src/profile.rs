use log::*;
use std::time::Instant;

/// A simple function for wrapping a code block in a profile statement
pub fn profile<T>(message: &str, f: impl FnOnce() -> T) -> T {
    let now = Instant::now();

    trace!("STARTING: {}", message);

    let result = f();

    trace!("COMPLETE: {} in {} ms", message, now.elapsed().as_millis());

    result
}
