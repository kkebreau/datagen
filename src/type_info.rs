// External
use anyhow::{anyhow, Result};
use chrono::{NaiveDate, NaiveDateTime, NaiveTime};

// Parsing
lalrpop_mod!(pub type_info);

#[derive(Debug, PartialEq, Clone)]
pub enum TypeInfo {
    Boolean {
        probability: f64,
    },
    Float {
        min: f64,
        max: f64,
    },
    ContinuousDistribution {
        // If present, specifies the names and indices of reference columns. The name should
        // correspond with the column name in the same file (not the CDF file).
        reference_columns: Option<Vec<(String, usize)>>,

        min_index: usize,
        max_index: usize,
        weight_index: usize,

        // Specifies the path to a CSV file which contains the CDF.
        path: String,
    },
    Datetime {
        min: NaiveDateTime,
        max: NaiveDateTime,
    },
    Date {
        min: NaiveDate,
        max: NaiveDate,
    },
    Time {
        min: NaiveTime,
        max: NaiveTime,
    },
    Integer {
        min: i64,
        max: i64,
    },
    Set {
        // If present, specifies the names and indices of reference columns. The name should
        // correspond with the column name in the same file (not the weights file).
        reference_columns: Option<Vec<(String, usize)>>,

        // Specifies the path to a CSV file which contains the set of values
        path: String,

        // Specifies the column index of the possible column values in the reference CSV file.
        column_index: usize,

        // If present, specifies the column index of a weight column in the reference CSV file.
        weight_index: Option<usize>,
    },
    I64Expression(I64Expression),
    F64Expression(F64Expression),
}

impl TypeInfo {
    pub fn new(expression: &str) -> Result<Self> {
        match type_info::TypeInfoExpressionParser::new().parse(expression) {
            Ok(parsed) => Ok(parsed),
            Err(e) => Err(anyhow!("{}", e)),
        }
    }
}

#[derive(Debug, PartialEq, Clone)]
pub enum I64Expression {
    Number(i64),
    Reference(String),
    RandomInteger { min: i64, max: i64 },
    Binary(Box<I64Expression>, Opcode, Box<I64Expression>),
}

#[derive(Debug, PartialEq, Clone)]
pub enum F64Expression {
    Number(f64),
    Reference(String),
    RandomFloat { min: f64, max: f64 },
    Binary(Box<F64Expression>, Opcode, Box<F64Expression>),
}

#[derive(Debug, PartialEq, Clone)]
pub enum Opcode {
    Mul,
    Div,
    Add,
    Sub,
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn type_info_parsing() {
        let type_info = TypeInfo::new("IntegerExpression(1 + Integer(20..40))").unwrap();
        assert_eq!(
            format!("{:?}", type_info),
            "I64Expression(Binary(Number(1), Add, RandomInteger { min: 20, max: 40 }))"
        );

        let type_info = TypeInfo::new("FloatExpression(1.0 + Float(20.0..40.0))").unwrap();
        assert_eq!(
            format!("{:?}", type_info),
            "F64Expression(Binary(Number(1.0), Add, RandomFloat { min: 20.0, max: 40.0 }))"
        );

        let type_info = TypeInfo::new("IntegerExpression(1 * Reference(a))").unwrap();
        assert_eq!(
            format!("{:?}", type_info),
            "I64Expression(Binary(Number(1), Mul, Reference(\"a\")))"
        );

        let type_info = TypeInfo::new("FloatExpression(1.0 * Reference(a))").unwrap();
        assert_eq!(
            format!("{:?}", type_info),
            "F64Expression(Binary(Number(1.0), Mul, Reference(\"a\")))"
        );
    }

    #[test]
    fn parse_integer_test() {
        let tree = type_info::TypeInfoExpressionParser::new()
            .parse("Integer(1..2)")
            .unwrap();
        assert_eq!(format!("{:?}", tree), "Integer { min: 1, max: 2 }")
    }

    #[test]
    fn parse_float_test() {
        let tree = type_info::TypeInfoExpressionParser::new()
            .parse("Float(1.0..2.0)")
            .unwrap();
        assert_eq!(format!("{:?}", tree), "Float { min: 1.0, max: 2.0 }")
    }

    #[test]
    fn parse_continuous_distribution_test() {
        let tree = type_info::TypeInfoExpressionParser::new()
            .parse("ContinuousDistribution(path.csv, [(col1, 1)], 1, 2, 3)")
            .unwrap();
        assert_eq!(
            tree,
            TypeInfo::ContinuousDistribution {
                reference_columns: Some(vec![("col1".to_owned(), 1)]),
                min_index: 1,
                max_index: 2,
                weight_index: 3,
                path: "path.csv".to_owned()
            }
        )
    }

    #[test]
    fn parse_set_test() {
        let tree = type_info::TypeInfoExpressionParser::new()
            .parse("Set(path.csv, [(col1, 1)], 1, 2)")
            .unwrap();
        assert_eq!(
            tree,
            TypeInfo::Set {
                reference_columns: Some(vec![("col1".to_owned(), 1)]),
                column_index: 1,
                weight_index: Some(2),
                path: "path.csv".to_owned()
            }
        );

        let tree = type_info::TypeInfoExpressionParser::new()
            .parse("Set(path.csv, 0, 1)")
            .unwrap();
        assert_eq!(
            tree,
            TypeInfo::Set {
                reference_columns: None,
                column_index: 0,
                weight_index: Some(1),
                path: "path.csv".to_owned()
            }
        );

        let tree = type_info::TypeInfoExpressionParser::new()
            .parse("Set(path.csv)")
            .unwrap();
        assert_eq!(
            tree,
            TypeInfo::Set {
                reference_columns: None,
                column_index: 0,
                weight_index: None,
                path: "path.csv".to_owned()
            }
        );

        let tree = type_info::TypeInfoExpressionParser::new()
            .parse("Set(path.csv, 1)")
            .unwrap();
        assert_eq!(
            tree,
            TypeInfo::Set {
                reference_columns: None,
                column_index: 1,
                weight_index: None,
                path: "path.csv".to_owned()
            }
        );
    }

    #[test]
    fn parse_date_test() {
        let tree = type_info::TypeInfoExpressionParser::new()
            .parse("Date(2020-01-01..2021-01-01)")
            .unwrap();
        assert_eq!(
            format!("{:?}", tree),
            "Date { min: 2020-01-01, max: 2021-01-01 }"
        )
    }

    #[test]
    fn parse_time_test() {
        let tree = type_info::TypeInfoExpressionParser::new()
            .parse("Time(00:00:00..23:59:59)")
            .unwrap();
        assert_eq!(
            format!("{:?}", tree),
            "Time { min: 00:00:00, max: 23:59:59 }"
        )
    }

    #[test]
    fn parse_datetime_test() {
        let tree = type_info::TypeInfoExpressionParser::new()
            .parse("Datetime(2020-01-01 00:00:00..2021-01-01 23:59:59)")
            .unwrap();
        assert_eq!(
            format!("{:?}", tree),
            "Datetime { min: 2020-01-01T00:00:00, max: 2021-01-01T23:59:59 }"
        )
    }

    #[test]
    fn parse_integer_expression_test() {
        let tree = type_info::TypeInfoExpressionParser::new()
            .parse("IntegerExpression(100 + Integer(20..40))")
            .unwrap();
        assert_eq!(
            format!("{:?}", tree),
            "I64Expression(Binary(Number(100), Add, RandomInteger { min: 20, max: 40 }))",
        );

        let tree = type_info::TypeInfoExpressionParser::new()
            .parse("IntegerExpression(100)")
            .unwrap();
        assert_eq!(format!("{:?}", tree), "I64Expression(Number(100))",);

        let tree = type_info::TypeInfoExpressionParser::new()
            .parse("IntegerExpression(100 * Reference(other_column))")
            .unwrap();
        assert_eq!(
            format!("{:?}", tree),
            "I64Expression(Binary(Number(100), Mul, Reference(\"other_column\")))",
        );
    }

    #[test]
    fn parse_float_expression_test() {
        let tree = type_info::TypeInfoExpressionParser::new()
            .parse("FloatExpression(100.0 + Float(20.0..40.0))")
            .unwrap();
        assert_eq!(
            format!("{:?}", tree),
            "F64Expression(Binary(Number(100.0), Add, RandomFloat { min: 20.0, max: 40.0 }))",
        );

        let tree = type_info::TypeInfoExpressionParser::new()
            .parse("FloatExpression(100.0)")
            .unwrap();
        assert_eq!(format!("{:?}", tree), "F64Expression(Number(100.0))",);

        let tree = type_info::TypeInfoExpressionParser::new()
            .parse("FloatExpression(100.0 * Reference(other_column))")
            .unwrap();
        assert_eq!(
            format!("{:?}", tree),
            "F64Expression(Binary(Number(100.0), Mul, Reference(\"other_column\")))",
        );
    }

    #[test]
    fn integer_parsing() {
        let tree = type_info::TypeInfoExpressionParser::new()
            .parse("IntegerExpression(22 + 2 * 8)")
            .unwrap();
        assert_eq!(
            format!("{:?}", tree),
            "I64Expression(Binary(Number(22), Add, Binary(Number(2), Mul, Number(8))))"
        );

        let tree = type_info::TypeInfoExpressionParser::new()
            .parse("IntegerExpression(22 + 2 * 8 + Reference(a))")
            .unwrap();
        assert_eq!(
            format!("{:?}", tree),
            "I64Expression(Binary(Binary(Number(22), Add, Binary(Number(2), Mul, Number(8))), Add, Reference(\"a\")))"
        );

        let tree = type_info::TypeInfoExpressionParser::new()
            .parse("IntegerExpression(2 * Integer(1..2))")
            .unwrap();
        assert_eq!(
            format!("{:?}", tree),
            "I64Expression(Binary(Number(2), Mul, RandomInteger { min: 1, max: 2 }))"
        );
    }

    #[test]
    fn float_parsing() {
        let tree = type_info::TypeInfoExpressionParser::new()
            .parse("FloatExpression(22.0 + 2.0 * 8.0)")
            .unwrap();
        assert_eq!(
            format!("{:?}", tree),
            "F64Expression(Binary(Number(22.0), Add, Binary(Number(2.0), Mul, Number(8.0))))"
        );

        let tree = type_info::TypeInfoExpressionParser::new()
            .parse("FloatExpression(22.3 + 2.0 * 8.0 + Reference(a))")
            .unwrap();
        assert_eq!(
            format!("{:?}", tree),
            "F64Expression(Binary(Binary(Number(22.3), Add, Binary(Number(2.0), Mul, Number(8.0))), Add, Reference(\"a\")))"
        );

        let tree = type_info::TypeInfoExpressionParser::new()
            .parse("FloatExpression(2.0 * Float(1.0..2.0))")
            .unwrap();
        assert_eq!(
            format!("{:?}", tree),
            "F64Expression(Binary(Number(2.0), Mul, RandomFloat { min: 1.0, max: 2.0 }))"
        );

        let tree = type_info::TypeInfoExpressionParser::new()
            .parse("FloatExpression(Float(200.0..400.0) * Float(20.0..40.0))")
            .unwrap();
        assert_eq!(
            format!("{:?}", tree),
            "F64Expression(Binary(RandomFloat { min: 200.0, max: 400.0 }, Mul, RandomFloat { min: 20.0, max: 40.0 }))"
        );

        let tree = type_info::TypeInfoExpressionParser::new()
            .parse("FloatExpression(Float(200.0..400.0) * 2.0)")
            .unwrap();
        assert_eq!(
            format!("{:?}", tree),
            "F64Expression(Binary(RandomFloat { min: 200.0, max: 400.0 }, Mul, Number(2.0)))"
        );
    }
}
