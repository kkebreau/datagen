// // External
// use nom::{
//     branch::alt,
//     bytes::complete::{is_a, is_not, tag},
//     character::complete::{char, digit1, space0},
//     combinator::{map, opt},
//     error::{context, VerboseError},
//     multi::{fold_many1, many1},
//     number::complete::float,
//     sequence::{preceded, terminated, tuple},
//     Err as NomErr, IResult,
// };
// use serde::{Deserialize, Serialize};
// use std::iter::Peekable;
// use std::vec::IntoIter;
// use anyhow::{anyhow, Result};
//
// // Internal
// use crate::parse::{parse_i64, Res};
//
// #[derive(Deserialize, Serialize, Clone, Debug, PartialEq)]
// pub enum Expression {
//     I64(I64Expression),
// }
//
// #[derive(Deserialize, Serialize, Clone, Debug, PartialEq)]
// pub enum I64Expression {
//     Constant(i64),
//     // Refers to the specific value of another column
//     ColumnValue(String),
//     Binary {
//         left: Box<I64Expression>,
//         op: BinaryOperator,
//         right: Box<I64Expression>,
//     },
// }
//
// #[derive(Deserialize, Serialize, Clone, Debug, PartialEq)]
// pub enum Constant {
//     I64(i64),
// }
//
// #[derive(Deserialize, Serialize, Clone, Debug, PartialEq)]
// pub enum BinaryOperator {
//     Plus,
//     Minus,
//     Divide,
//     Multiply,
// }
//
// impl BinaryOperator {
//     // Higher precedence should be done first
//     pub fn precedence(self) -> usize {
//         match self {
//             BinaryOperator::Minus | BinaryOperator::Plus => 1,
//             BinaryOperator::Divide | BinaryOperator::Multiply => 2,
//         }
//     }
// }
//
// fn i64_expression(input: &str) -> Res<&str, I64Expression> {
//     println!("I64Expression? {:?}", input);
//     context("i64_expression", alt((i64_binary, i64_constant)))(input)
// }
//
// fn i64_constant(input: &str) -> Res<&str, I64Expression> {
//     println!("CONSTANT? {:?}", input);
//     context("i64_constant", parse_i64)(input).map(|(next_input, result)| {
//         println!("YES! Remainder: {}", next_input);
//         (next_input, I64Expression::Constant(result))
//     })
// }
//
// fn binary_operator(input: &str) -> Res<&str, BinaryOperator> {
//     context(
//         "binary_operator",
//         terminated(
//             preceded(space0, alt((tag("+"), tag("-"), tag("*"), tag("/")))),
//             space0,
//         ),
//     )(input)
//     .and_then(|(next, result)| {
//         let op = match result {
//             "+" => Ok(BinaryOperator::Plus),
//             "-" => Ok(BinaryOperator::Minus),
//             "*" => Ok(BinaryOperator::Multiply),
//             "/" => Ok(BinaryOperator::Divide),
//             _ => Err(NomErr::Error(VerboseError { errors: vec![] })),
//         }?;
//         Ok((next, op))
//     })
// }
//
// fn i64_tokens(input: &str) -> Res<&str, Vec<Token>> {
//     context(
//         "i64_tokens",
//         fold_many1(
//             preceded(
//                 space0,
//                 alt((
//                     map(binary_operator, Token::BinaryOperator),
//                     map(parse_i64, Token::Number),
//                     map(char('('), |_| Token::OpenParen),
//                     map(char(')'), |_| Token::CloseParen),
//                 )),
//             ),
//             vec![],
//             |mut acc, token| {
//                 acc.push(token);
//                 acc
//             },
//         ),
//     )(input)
// }
//
// struct BinaryExpression<'a> {
//     left: &'a str,
//     operator: &'a str,
//     right: &'a str,
// }
//
// struct ExpressionBuilder {
//     token_iter: Peekable<IntoIter<Token>>,
// }
//
// #[derive(Clone)]
// enum Token {
//     OpenParen,
//     CloseParen,
//     Number(i64),
//     BinaryOperator(BinaryOperator),
// }
//
// impl ExpressionBuilder {
//     fn start(&mut self, token: Token) -> Result<I64Expression> {
//         let res = match token {
//             Token::Number(n) => Ok(I64Expression::Constant(n)),
//             Token::BinaryOperator(_) => Err(anyhow!("Operator not valid at this point.")),
//             Token::OpenParen => {
//                 let internal_tokens = vec![];
//                 // logic: parse until close paren for the current open paren, then try to make an
//                 // expression from the result
//                 let mut open_paren_count = 1;
//
//                 while let Some(token) = &self.token_iter.next() {
//                     match token {
//                         Token::OpenParen => open_paren_count += 1,
//                         Token::CloseParen => {
//                             match open_paren_count {
//                                 // We have reached the end of the original expression, so we can
//                                 // build an expression from the internal_tokens
//                                 1 => {
//                                     return self.from_tokens(internal_tokens);
//                                 }
//
//                                 // Overflow; too many close parens
//                                 0 => {
//                                 }
//                             }
//                             open_paren_count -= 1;
//                         }
//                         _ => {}
//                     }
//                     internal_tokens.push(token.clone());
//                 }
//
//                 Err(anyhow!("Unmatched opening paren"))
//             }
//             Token::CloseParen => Err(anyhow!("CloseParen not valid at this point."))
//         };
//
//         Ok(res?)
//     }
//
//     // Should include an entire expression
//     fn from_tokens(&mut self, tokens: Vec<Token>) -> Result<I64Expression> {
//
//     }
//
//     pub fn build_expression(&mut self, input: &str) -> Res<&str, I64Expression> {
//         let (remainder, tokens) = i64_tokens(input)?;
//
//         let first = self.token_iter.next()?;
//         let mut left =
//     }
// }
//
// fn i64_binary(input: &str) -> Res<&str, I64Expression> {
//     println!("I64BINARY? {:?}", input);
//     // let non_operator_expressions = vec![];
//     let (remainder, operator) = binary_operator(input)?;
//     println!("YES! Remainder: {}", remainder);
//     //     let (left, op, right) = result;
//     //     Ok((
//     //         next_input,
//     //         I64Expression::Binary {
//     //             left: Box::new(left),
//     //             op,
//     //             right: Box::new(right),
//     //         },
//     //     ))
//     // })
//     Err(NomErr::Error(VerboseError { errors: vec![] }))
// }
//
// #[cfg(test)]
// mod tests {
//     use super::*;
//
//     #[test]
//     fn parse_i64_expression() {
//         assert_eq!(i64_expression("1"), Ok(("", I64Expression::Constant(1))));
//         assert_eq!(
//             i64_expression("1 + 1"),
//             Ok((
//                 "",
//                 I64Expression::Binary {
//                     left: Box::new(I64Expression::Constant(1)),
//                     op: BinaryOperator::Plus,
//                     right: Box::new(I64Expression::Constant(1)),
//                 }
//             ))
//         );
//     }
// }
